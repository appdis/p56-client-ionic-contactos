export const environment = {
  production: true,

  firebaseConfig: {
    apiKey: "AIzaSyBz0Su8TiaZk3gckmwwPP8tRLJuZJfrwYY",
    authDomain: "mi-cafeteria-2020.firebaseapp.com",
    databaseURL: "https://mi-cafeteria-2020.firebaseio.com",
    projectId: "mi-cafeteria-2020",
    storageBucket: "mi-cafeteria-2020.appspot.com",
    messagingSenderId: "100408865452",
    appId: "1:100408865452:web:2aee0ff11460c92f0b6509"
  },

  googleWebClientId: '100408865452-6hd5eag84gupjuc7bpfrg7h6h4tg3bn2.apps.googleusercontent.com'
};
