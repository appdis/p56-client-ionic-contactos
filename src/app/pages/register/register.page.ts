import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators, AbstractControl} from "@angular/forms";
import {NavController, AlertController} from "@ionic/angular";
import {Router} from "@angular/router";
import { MessageService } from 'src/app/shared/services/message.service';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: "app-register",
  templateUrl: "./register.page.html",
  styleUrls: ["./register.page.scss"]
})
export class RegisterPage implements OnInit {
  
  public signupForm: FormGroup;
  showPassword: boolean = false;
  public typePassword: any = "password"

  constructor(
    public navCtrl: NavController, 
    private router: Router, 
    public formBuilder: FormBuilder, 
    public alertCtrl: AlertController,
    public auth: AuthService, 
    public message: MessageService, 
  ) {
    this.signupForm = formBuilder.group(
      {
        name: ["", Validators.compose([Validators.required])],
        email: ["", Validators.compose([Validators.required, Validators.email])],
        password: ["", Validators.compose([Validators.minLength(6), Validators.required])],
        confirmPassword: ["", Validators.compose([Validators.minLength(6), Validators.required])]
      },
      {
        validator: this.MatchPassword
      }
    );
  }//end constructor

  ngOnInit() {}

  password(){
    this.showPassword = !this.showPassword;
    if(this.showPassword) {
      this.typePassword = "text";
    } else {
      this.typePassword = "password";
    }
  }

  async signUpUser() {
    if (!this.signupForm.valid) {
      this.message.alert("Error en validación de datos ingresados, reviselos por favor...");
    } else {
      this.message.showLoader();
      let error: any = await this.auth.signupUser(this.signupForm.value.name, this.signupForm.value.email, this.signupForm.value.password);

      if (error === undefined) {
        this.message.hideLoader();
        this.router.navigate(["login"]);
      } else {
        this.message.hideLoaderWithAlert(error.message);
      }//end if
    }//end if
  }//end signUpUser

  login() {
    this.router.navigate(['login'])
  }//end login

  /****************************************** VALIDATORS ******************************************/

  MatchPassword(AC: AbstractControl) {
    let password = AC.get("password").value;
    let confirmPassword = AC.get("confirmPassword").value;
    if (password != confirmPassword) {
      AC.get("confirmPassword").setErrors({matchpassword: true});
    } else {
      return null;
    } //end if
  } //end MatchPassword
} //end class

