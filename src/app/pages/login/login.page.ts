import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username:string;
  password:string;

  constructor(public auth: AuthService,
    public router: Router) { }

  ngOnInit() {
  }

  async loginGoogle() {
    let error: any = await this.auth.googleLogin();
    
    if (error === undefined){
      this.router.navigate(['tabs']);
    }else{     
      alert(JSON.stringify(error));
    }
  }

  async loginEmail(){
    let error: any = await this.auth.emailPasswordLogin(this.username, this.password);
    if (error === undefined) {
      this.router.navigate(['tabs']);
      this.username = '';
      this.password = '';
    } else {
      console.error("Error",error)
      alert(JSON.stringify(error));
    }
  }

  register(){
    this.router.navigate(['register']);
  }

}
