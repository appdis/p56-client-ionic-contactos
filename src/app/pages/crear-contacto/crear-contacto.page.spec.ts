import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrearContactoPage } from './crear-contacto.page';

describe('CrearContactoPage', () => {
  let component: CrearContactoPage;
  let fixture: ComponentFixture<CrearContactoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearContactoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrearContactoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
