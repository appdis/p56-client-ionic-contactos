import { Component, OnInit } from '@angular/core';
import { ContactosService } from 'src/app/shared/services/contactos.service';
import { Persona } from 'src/app/shared/model/persona';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-crear-contacto',
  templateUrl: './crear-contacto.page.html',
  styleUrls: ['./crear-contacto.page.scss'],
})
export class CrearContactoPage implements OnInit {

  persona: Persona = new Persona();
  constructor(private contactosService: ContactosService,
    private toastController: ToastController) { }

  ngOnInit() {
  }

  guardar(){
    this.contactosService.sendContacto(this.persona).subscribe(data => {
      console.log(data);
      if(data.code = '1')
        this.toast("Guardado satisfactorio");
    });   
  }

  

  async toast(text: string, duration: number = 2000, position?) {
    const toast = await this.toastController.create({
      message: text,
      position: position || "middle",
      duration: duration
    });
    await toast.present();
  }
}
