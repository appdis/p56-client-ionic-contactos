import { Component, OnInit } from '@angular/core';
import { ContactosService } from 'src/app/shared/services/contactos.service';
import { Observable } from 'rxjs';
import { Persona } from 'src/app/shared/model/persona';

@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.page.html',
  styleUrls: ['./contactos.page.scss'],
})
export class ContactosPage implements OnInit {

  personas: Observable<Persona[]>;

  constructor(private contactosService: ContactosService) { }

  ngOnInit() {
    this.personas = this.contactosService.getContactos();
  }
}
