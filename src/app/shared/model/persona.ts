
export class Persona {
    public cedula: string;
    public nombres: string;
    public direccion: string;
    public telefonos: []
}
