import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Respuesta } from '../model/respuesta';
import { Persona } from '../model/persona';

@Injectable({
  providedIn: 'root'
})
export class ContactosService {

  private SEND_CONTACTO_URL:string = 'http://localhost:8080/test-jpa/srv/personas/insertar';
  private GET_CONTACTOS_URL:string = 'http://localhost:8080/test-jpa/srv/personas/listado';

  constructor(private http: HttpClient) { }

  sendContacto(contacto: Persona): Observable<any> {
    return this.http.post<Respuesta>(this.SEND_CONTACTO_URL, contacto);
  }

  getContactos(): Observable<any[]> {
    return this.http.get<Persona[]>(this.GET_CONTACTOS_URL);
  }
}
