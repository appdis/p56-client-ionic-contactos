import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  loadingPopup: any;
  isLoading = false;
  loaderToShow: any;

  constructor(public alertController: AlertController, 
    public loadingController: LoadingController, 
    private toastController: ToastController) { }

  showAutoHideLoader() {
    this.loadingController
      .create({
        message: "Procesando",
        duration: 20000
      })
      .then(res => {
        res.present();
        res.onDidDismiss().then(dis => {
          console.error("Close");
        });
      });
  } //end showAutoHideLoader

  async showLoader() {
    this.loaderToShow = await this.loadingController.create({
      message: "Procesando...",
      backdropDismiss: true
    });
    await this.loaderToShow.present();
  } //end showLoader

  async showLoaderWithOutDismiss() {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 5000,
      message: "Actualizando, por favor espere un momento...",
      backdropDismiss: true
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss().then(() => console.error('Dismissed'));
        }//end if
      });
    });

  } //end showLoaderWithOutDismiss

  hideLoader() {
    let time = true;
    try {
      if (time == true) {
        setTimeout(() => {
          this.loaderToShow.dismiss(null, undefined);
        }, 2000);
      } else {
        this.loaderToShow.dismiss(null, undefined);
      }
    } catch (error) { }
  } //end hideLoader

  async hideLoaderNow() {
    this.isLoading = false;
    return await this.loadingController.dismiss();
  } //end hideLoader

  hideLoaderFast() {
    try {
      setTimeout(() => {
        this.loaderToShow.dismiss(null, undefined);
      }, 100);
    } catch (error) { }
  } //end hideLoader

  async hideLoaderWithAlert(msg: string) {
    try {
      setTimeout(() => {
        this.loadingController.dismiss(null, undefined);
        this.alert(msg);
      }, 2000);
    } catch (error) { }
  } //end hideLoader

  async alert(msn) {
    let alert = await this.alertController.create({
      message: msn,
      //backdropDismiss: false,
      buttons: ["OK"]
    });
    await alert.present();
  } //end alert

  async message(title, msn, cb?) {
    let alert = await this.alertController.create({
      header: title,
      message: msn,
      //backdropDismiss: false,
      buttons: [
        {
          text: "Aceptar",
          handler: () => {
            //alert.;
            if (cb !== undefined) cb();
          }
        }
      ]
    });
    await alert.present();
  } //end alert

  async messageAddress(title, msn, cb?) {
    let alert = await this.alertController.create({
      header: title,
      message: msn,
      //backdropDismiss: false,
      buttons: [
        {
          text: "Seleccionar",
          handler: () => {
            //alert.;
            if (cb !== undefined) cb();
          }
        }
      ]
    });
    await alert.present();
  } //end alert

  async notify(title, cb?) {
    let alert = await this.alertController.create({
      message: title,
      buttons: [
        {
          text: "Aceptar",
          handler: () => {
            //alert.;
            cb();
          }
        }
      ]
    });
    alert.present();
    setTimeout(() => {
      alert.dismiss();
      cb();
    }, 2000);
  } //end notify

  async toast(text: string, duration: number = 2000, position?) {
    const toast = await this.toastController.create({
      message: text,
      position: position || "middle",
      duration: duration
    });
    await toast.present();
  } //end toast

  async confirmAction(title: string, text: string, cb?) {
    let alert = await this.alertController.create({
      header: title,
      message: text,
      buttons: [
        {
          text: "No",
          role: "cancel",
          handler: () => { }
        },
        {
          text: "Si",
          handler: () => {
            cb();
          }
        }
      ]
    });
    alert.present();
  }

  async inputAction(title: string, text: string, cb?) {
    let alert = await this.alertController.create({
      header: title,
      message: text,
      inputs: [
        {
          name: "justification",
          placeholder: "ingrese la razón..."
        }
      ],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: data => { }
        },
        {
          text: "Confirmar",
          handler: data => {
            cb(data.justification);
          }
        }
      ]
    });
    alert.present();
  }
}
